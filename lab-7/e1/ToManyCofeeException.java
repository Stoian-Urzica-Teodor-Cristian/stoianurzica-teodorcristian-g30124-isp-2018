package StoianUrzica.TeodorCristian.ISP2018.lab7.e1;

class ToManyCofeeException extends Exception {
    public ToManyCofeeException() {
    }

    public ToManyCofeeException(String msg) {
        super(msg);
    }
}
