package StoianUrzica.TeodorCristian.ISP2018.lab7.e1;

public class CofeeMaker {
    private int cofeeNumber;

    public CofeeMaker(){
        cofeeNumber = 0;
    }

    Cofee makeCofee() throws ToManyCofeeException{
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        if(cofeeNumber < 3) {
            System.out.println("Make a coffe");
            cofeeNumber++;
            Cofee cofee = new Cofee(t, c);
            return cofee;
        }
        else {
            throw new ToManyCofeeException("Too many cofees!!!!");
        }
    }
}
