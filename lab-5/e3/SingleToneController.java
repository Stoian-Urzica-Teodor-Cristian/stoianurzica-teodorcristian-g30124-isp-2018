package StoianUrzica.TeodorCristian.ISP2018.lab5.e3;

public class SingleToneController{
    private static Controller singleController;

    private SingleToneController(){
        System.out.println("Controller created!");
    }

    public static Controller createController(){
        if (singleController != null){
            singleController = new Controller();
        }
        else{
            System.out.println("Singletone Warning!Only one controller alowed.");
        }
        return singleController;
    }
}
