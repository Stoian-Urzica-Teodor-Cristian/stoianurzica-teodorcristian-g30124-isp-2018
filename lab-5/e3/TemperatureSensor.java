package StoianUrzica.TeodorCristian.ISP2018.lab5.e3;

import java.util.Random;

import static java.lang.Math.abs;

public class TemperatureSensor extends Sensor {

    private Random rdn = new Random();

    public TemperatureSensor(String location){
        super(location);
    }

    @Override
    public int readValue(){
        return abs(rdn.nextInt() % 100);
    }
}
