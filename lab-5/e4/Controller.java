package StoianUrzica.TeodorCristian.ISP2018.lab5.e4;

import java.util.concurrent.TimeUnit;

public class Controller {
    private Sensor ls = new LightSensor("l1");
    private Sensor ts = new TemperatureSensor("l2");

    public void display(){
        for(int i=0; i<20; i++){
            try {
                TimeUnit.SECONDS.sleep(1);
                System.out.println("Light sensor: "+ls.readValue());
                System.out.println("Light sensor: "+ts.readValue());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args){
        Controller c = new Controller();
        c.display();
    }
}
