package StoianUrzica.TeodorCristian.ISP2018.lab5.e4;

import java.util.Random;

import static java.lang.Math.abs;

public class LightSensor extends Sensor {

    private Random rdn = new Random();

    public LightSensor(String location){
        super(location);
    }

    @Override
    public int readValue(){
        return abs(rdn.nextInt() % 100);
    }
}
