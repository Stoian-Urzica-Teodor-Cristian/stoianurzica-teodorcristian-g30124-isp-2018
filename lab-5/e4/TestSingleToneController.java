package StoianUrzica.TeodorCristian.ISP2018.lab5.e4;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class TestSingleToneController {

    @Test
    public void testSingleToneController(){
        Controller c1 = SingleToneController.createController();
        Controller c2 = SingleToneController.createController();
        assertNotNull(c1);
        assertEquals(c1,c2); //Compare adresses
    }
}
