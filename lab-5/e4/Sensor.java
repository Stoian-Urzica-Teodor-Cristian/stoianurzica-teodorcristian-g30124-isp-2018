package StoianUrzica.TeodorCristian.ISP2018.lab5.e4;

public abstract class Sensor {
    protected String location;

    public Sensor(String location) {
        this.location = location;
    }

    public int readValue(){
        return 0;
    }

    public String getLocation() {
        return location;
    }

}
