package StoianUrzica.TeodorCristian.ISP2018.lab5.e4;

public class SingleToneController extends Controller{
    private static Controller singleController = null;

    private SingleToneController(){
        super();
        System.out.println("Controller created!");
    }

    public static Controller createController(){
        if (singleController == null){
            singleController = new SingleToneController();
        }
        else{
            System.out.println("Singletone Warning!Only one controller alowed.");
        }
        return singleController;
    }
}
