package StoianUrzica.TeodorCristian.ISP2018.lab5.e2;

public class RotatedImage implements Image {
    private RealImage realImage;
    private String fileName;

    public RotatedImage(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void display() {
        System.out.println("Display rotated" + this.fileName);
    }
}
