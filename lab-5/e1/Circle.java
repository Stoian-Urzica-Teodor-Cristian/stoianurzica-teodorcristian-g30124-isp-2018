package StoianUrzica.TeodorCristian.ISP2018.lab5.e1;

public class Circle extends Shape {
    protected double radius;

    public Circle(){
        super();
    }

    public Circle(double radius, String color, boolean filled){
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea(){
        return 2 * 3.14 * this.radius * this.radius;
    }

    @Override
    public  double getPerimetter(){
        return 2 * 3.14 *this.radius;
    }

    @Override
    public String toString(){
        return "Circle that extends " + super.toString() + " with radius = " + this.radius;
    }
}
