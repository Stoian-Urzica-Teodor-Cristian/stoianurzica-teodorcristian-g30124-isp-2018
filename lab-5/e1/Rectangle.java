package StoianUrzica.TeodorCristian.ISP2018.lab5.e1;

public class Rectangle extends Shape {

    protected double length;
    protected double width;

    public Rectangle(){
        super();
        this.length = 0;
        this.width = 0;
    }

    public Rectangle(double length, double width){
        super();
        this.length = length;
        this.width = width;
    }

    public Rectangle(double length, double width, String color, boolean filled){
        super(color, filled);
        this.length = length;
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public double getArea(){
        return this.width * this.length;
    }

    @Override
    public double getPerimetter(){
        return this.length * 2 + this.width *2;
    }

    @Override
    public String toString(){
        return "Rectangle that extends " + super.toString() + "with width = " + this.width + " and length = " + this.length;
    }
}
