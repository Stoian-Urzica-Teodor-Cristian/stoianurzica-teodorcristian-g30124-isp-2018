package StoianUrzica.TeodorCristian.ISP2018.lab9.e4;

import javax.swing.*;

public class Robot extends Thread {
    private int id;
    private int x;
    private int y;

    Robot(int id, int x, int y){
        this.id = id;
        this.x = x;
        this.y = y;
        System.out.println("Robotul cu id-ul: " + id + "la x = " + x + "y = " + y);
    }

    public void run(){
        synchronized (Manipulator.matrix) {
            Manipulator.matrix[x][y] -= 1;
        }
        int direction = ((int)Math.round(Math.random()*100))%4;
        if (direction == 0)
            x+=1;
        if (direction == 1)
            x-=1;
        if (direction == 2)
            y+=1;
        if (direction == 3)
            y-=1;

        synchronized (Manipulator.matrix) {
            Manipulator.matrix[x][y] -= 1;
        }
    }
}
