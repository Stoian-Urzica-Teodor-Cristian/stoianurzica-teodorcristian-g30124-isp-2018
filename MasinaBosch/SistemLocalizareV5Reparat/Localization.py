import PathCalculator2 as PC
#import CarClient as CC
import ReadMap as RM
import math
import copy
import time
from random import randint
from threading import Thread
import SerialHandler
import SaveEncoder
import threading,serial,time,sys
import cv2 as cv2
import numpy as np
import CarClient as CC
global serialHandler
global ev1
global pwm

serialHandler = SerialHandler.SerialHandler()
ev1 = threading.Event()
print(serialHandler.readThread.addWaiter("ENPB",ev1))
sent = serialHandler.sendEncoderActivation(False)
sent = serialHandler.sendEncoderActivation(True)
encoderValue = 0.001
pwm = 7
global initializeComplete
initializeComplete = False

class Localization(Thread):

        FLAG_FORWARD = 0
        FLAG_RIGHT = 2
        FLAG_LEFT = 1
        FLAG_STOP = 3
        
        def __init__(self, threadID, name):
                Thread.__init__(self)
                self.threadID = threadID
                self.divisionNumber = 0
                self.isInitialized = False
                self.name = name
                self.speed = 0 # m/s
                self.REFERENCE_SPEED = 0.459
                self.REFERENCE_DISTANCE = 1
                self.REFERENCE_ENCODER_SPEED = 16.9
                self.raza1 = 0
                self.raza2 = 0
                self.flag = 0
                self.delta_t = 0
                self.delta_r = 0
                self.ORIGIN = [0,0]
                self.current_index = 0
                self.next_index = 1
                self.encoderValue = 0
                self.map = RM.Map('harta.json')
                self.pathCalculator = PC.PathCalculator(self.map.getNodes())
                self.path = self.pathCalculator.getOptimalPath(self.map.getNodeByCoordinates(0.9,1.8), self.map.getNodeByCoordinates(0.9,0.9))
                #self.path = self.pathCalculator.getOptimalPath(self.map.getNodeByCoordinates(0,1.35), self.map.getNodeByCoordinates(2.25,1.35))
                print('Current path: ' + str(self.path) + " " +str(len(self.path)))
                self.current_position = self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()
                self.next_position = self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()
                self.current_time = time.time() % 60
                self.GPS_TIME = 1
                self.GPS_PREV_TIME = 0
                self.PREV_TIME = 0
                self.gpsPosition = [CC.car_poz.real/10, CC.car_poz.imag/10]
                #self.gpsPosition = [0,0]
                #print(self.gpsPosition)
                ##GRAPHICS##
                self.bck = np.zeros((512,512,3), np.uint8)
                time.sleep(0.1)
                self.nodesList = []
                #for i in self.path:
                 #       self.nodesList.append(self.pathCalculator.getNodeByName(i[0]))
                for i in self.map.getNodes():
                        self.nodesList.append(i)
                #print(self.nodesList)

        def getAproximatedCoordinate(self, val):
                val = val / 0.45
                math.trunc(val)
                print(val)

        #PARKING NODE FUNCTION
        def getParkNode(self):
                for node in self.map.getNodes():
                        #print(node.getNeibourgh())
                        if ((node.getNeibourgh()[0] == 0) and (node.getNeibourgh()[1] == 0) and (node.getNeibourgh()[2] == 0) and (node.getNeibourgh()[3] == 0)):
                                return node
                print('No praking node!')
                return 0

        #PARKIGN CLOSE CHECK
        def checkIsCloseToPark():
                if (self.pathCalculator.calculateEuclidianDistance(self.map.getNodeByCoordinates(self.position[0], self.position[1]), self.map.getNodeByCoordinates(self.parkingNode.getCoord()[0],self.parkingNode.getCoord()[1])) < self.CONST_PARKING_MINIMUM_DISTANCE):
                        return True
                return False
                        
                
        def drawMap(self):
                key = cv2.waitKey(0) &0xFF
                #cv2.circle(self.bck (x,y), r, (r, g, b), -1)
                for i in self.nodesList:
                        #print('FOR')
                        cv2.circle(self.bck, (int(i.getCoord()[0] * 100 + 100), int(i.getCoord()[1] * 100 + 50)) ,15 , (0,255,255), 0)
                        cv2.imshow("Map", self.bck)
                        key = cv2.waitKey(1) &0xFF

        def redrawMap(self):
                cv2.circle(self.bck, (int(self.current_position[0] * 100 +100) , int(self.current_position[1] * 100 +50)), 15, (0,255,255), -1)
                cv2.imshow("Map", self.bck)
                key = cv2.waitKey(1) &0xFF
                           
        def dividePath(self):
                pass
        
        def reclaculate_path(self, x1,y1,x2,y2):
                self.path = self.pathCalculator.getOptimalPath(self.map.getNodeByCoordinates(x1,y1), self.map.getNodeByCoordinates(x2,y2))
                
                

        def calculate_delta_r(self,point_1,point_2):
                self.raza1=math.sqrt(math.pow(self.ORIGIN[0]-point_1[0],2)+math.pow(self.ORIGIN[1]-point_1[1],2))
                self.raza2=math.sqrt(math.pow(self.ORIGIN[0]-point_2[0],2)+math.pow(self.ORIGIN[1]-point_2[1],2))
                return math.fabs(self.raza1 - self.raza2)

        def isTimeToChange(self,t):
                if (t - self.PREV_TIME >= self.delta_t):
                        self.PREV_TIME = t
                        print('Conditie schimbare indeplinita')
                        return True
                return False

        #DE LUCRAT AICI
        def isTimeForGPSChange(self,t):
                #self.gpsPosition = gps.getPosition()
                if (t - self.GPS_PREV_TIME >= self.GPS_TIME):
                        self.GPS_PREV_TIME = t
                        self.gpsPosition = [CC.car_poz.real/10, CC.car_poz.imag/10]
                        self.distanceToCurrentPoint = math.sqrt(math.pow(self.gpsPosition[0]-self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()[0],2) +
                                                                math.pow(self.gpsPosition[1]-self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()[1],2))

                        self.distanceToNextPoint = math.sqrt(math.pow(self.gpsPosition[0]-self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()[0],2) +
                                                             math.pow(self.gpsPosition[1]-self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()[1],2))

                        if(self.distanceToCurrentPoint > self.distanceToNextPoint):
                                pass
                        else:
                                self.current_index += 1
                                self.next_index +=1
                                print('GPS a intrat la timp curent = ' + str(self.current_time) + 'si a mutat la ' + self.path[self.current_index][0])
                                self.current_time = time.time() % 60
                                self.current_position = self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()
                                self.next_position = self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()
                                self.delta_r = self.calculate_delta_r(self.current_position, self.next_position)
                                try:
                                        self.delta_t = self.delta_r / self.speed
                                except:
                                        print("T the same")
                                self.redrawMap()
                                self.GPS_PREV_TIME = time.time()
                        return True
                else:
                        return False

        def changePosition(self,t):
                if(self.isTimeToChange(t)):
                        print('Change position ')
                        t = time.time() % 60
                        self.current_time = t % 60
                        self.current_index +=1
                        self.next_index += 1
                        print('S-a modificat pozitia la timp curent = ' + str(t % 60) + ' la index = ' + self.path[self.current_index][0]);
                        try:
                                self.current_position = self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()
                                self.next_position = self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()
                        except:
                                print('Nu mai calculez indicii.')
                                serialHandler.sendMove(0,0)

        def isEndOfTheRoad(self):
                if(self.path[self.current_index][1] == "STOP"):
                        return True
                else:
                        return False

        def getRoadType(self):
            self.nodeFromPath = self.pathCalculator.getNodeByName(self.path[self.current_index][0])
            if(self.path[self.current_index][1] == 'STOP'):
                    self.flag = 3
                    return self.flag
                
            if(self.path[self.current_index][1] == 'LEFT'):
                    self.flag = 1

            if(self.path[self.current_index][1] == 'RIGHT'):
                    self.flag = 2
                    
            if(self.path[self.current_index][1] == 'FORWARD'):
                    self.flag = 0
                    
            return self.flag


        def getNextRoadType(self):
            self.nodeFromPath = self.pathCalculator.getNodeByName(self.path[self.current_index][0])
            if(self.path[self.next_index][1] == 'STOP'):
                    self.flag = 3
                    return self.flag
                
            if(self.path[self.next_index][1] == 'LEFT'):
                    self.flag = 1

            if(self.path[self.next_index][1] == 'RIGHT'):
                    self.flag = 2
                    
            if(self.path[self.next_index][1] == 'FORWARD'):
                    self.flag = 0
                    
            return self.flag

        def onAir(self):
                return self.isInitialized

        #PE BUTUCI TREBUIE DATE DIN TESTE
        def refreshSpeed(self):
                return self.encoderValue * self.REFERENCE_SPEED / self.REFERENCE_ENCODER_SPEED

        def getEncoderValue(self):
            try:
                message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                if(len(message)<=6):
                        message = serialHandler.getBufferSh()
                        
                if (message[0] == '@'):
                    numberMessage = ''
                    for i in message:
                            if (i in ['0','1','2','3','4','5','6','7','8','9','.',]):
                                    numberMessage = numberMessage + i;
                    #print(numberMessage)
                    #print("FLOAT: " + str(float(numberMessage)))
                    return float(numberMessage)
                else:
                    return 0
                            
            except:
                return self.encoderValue

        def run(self):
                print('Run')
                self.drawMap()
                self.redrawMap()
                time.sleep(1)
                self.t1 = time.time() % 60
                #print('Gata draw')
                self.delta_r = self.calculate_delta_r(self.current_position, self.next_position)
                self.isInitialized = True
                time.sleep(1)
                while True:
                        while (self.isEndOfTheRoad() == False):
                                #self.getEncoderValue()
                                self.encoderValue = self.getEncoderValue()
                                print('ENC: ' + str(self.encoderValue))
                                if(self.encoderValue!= 0):
                                        print(self.encoderValue)
                                        self.speed = self.refreshSpeed()
                                        self.delta_t = self.delta_r / self.speed
                                else:
                                        pass
                                if(self.isTimeForGPSChange(time.time() % 60) == False):
                                        if(self.speed!=0):
                                                self.changePosition(time.time() % 60)
                                                self.delta_r = self.calculate_delta_r(self.current_position, self.next_position)
                                                self.redrawMap()   
                                        elif(pwm == 0):
                                                self.GPS_PREV_TIME = time.time() % 60
                                                self.delta_t = 0
                                                self.delta_r = 0
                                else:
                                        print('GPS')

##TESTE##

print("#############################################")
print("###########    NEW TEST      ################")
print("#############################################")
serialHandler.startReadThread()

L = Localization('1', 'LocalizationThread')
L.start()
#pwm = 0
#angle = 0

while(L.onAir() == False):
        pass
print("Localization initialized")
#serialHandler.sendMove(7,0)
#time.sleep(0.7)
#serialHandler.sendMove(0,0)

#L.run()

'''
L e threadul de localizare
pwm trebuie sa fie o variabila globala sa vad daca merge sau nu masina
L.onAir() returneaza true cand threadul a fost initializat si poate fi folosit
L.getNextRoadType si L.getRoadType retuneza tipul tilei urmatoare sau curente(vezi flagurile)
Poti muta serialHandlerul din fisierul asta la tine in decizional sau il poti folosi de aici
Ca oricum nu il folosesc in codul localizarii, a fost doar pentru teste
PS: Daca scoti printul la encoder se poate sa nu mearga(HABARN NU A DE CE)...
Poate in codul mnare merge si fara, vezi tu
Daca ai vreo intrebare, suna-ma, spor!
'''
