import json;

class Node:
    def __init__(self,Name,BACK,RIGHT,AHEAD,LEFT,Coordinates,IN_BACK, IN_RIGHT, IN_AHEAD, IN_LEFT):
        self.name=Name
        self.coordinates=Coordinates
        self.neibourgh=[0,0,0,0]
        self.inNeibourgh=[0,0,0,0]

        if BACK.decode('UTF-8') == 'none':
            self.neibourgh[0]=0
        else:
            self.neibourgh[0] = str(BACK.decode('UTF-8'))
            
        if RIGHT.decode('UTF-8') == 'none':
            self.neibourgh[1]=0
        else:
            self.neibourgh[1]=str(RIGHT.decode('UTF-8'))
            
        if AHEAD.decode('UTF-8') == 'none':
            self.neibourgh[2]=0
        else:
            self.neibourgh[2]=str(AHEAD.decode('UTF-8'))
            
        if LEFT.decode('UTF-8') == 'none':
            self.neibourgh[3]=0
        else:
            self.neibourgh[3]=str(LEFT.decode('UTF-8'))
            
            #######IN#####

        if IN_LEFT.decode('UTF-8') == 'none':
            self.inNeibourgh[0]=0
        else:
            self.inNeibourgh[0]=str(IN_LEFT.decode('UTF-8'))
            
        if IN_RIGHT.decode('UTF-8') == 'none':
            self.inNeibourgh[1]=0
        else:
            self.inNeibourgh[1]=str(IN_RIGHT.decode('UTF-8'))
            
        if IN_AHEAD.decode('UTF-8') == 'none':
            self.inNeibourgh[2]=0
        else:
            self.inNeibourgh[2]=str(IN_AHEAD.decode('UTF-8'))
            
        if IN_BACK.decode('UTF-8') == 'none':
            self.inNeibourgh[3]=0
        else:
            self.inNeibourgh[3]=str(IN_BACK.decode('UTF-8'))

        
            
    def getName(self):
        return (self.name).decode('UTF-8')
	
    def getNeibourgh(self):
        return (self.neibourgh)

    def getInNeibourgh(self):
        return self.inNeibourgh
	
    def getCoord(self):
        return (self.coordinates)
	
    def getNeibourghByPosition(self, pos):
        return (self.neibourgh[pos]).decode('UTF-8')
            
		
class Map:
    def __init__(self, fileName):
        self.jsonMap=[]

        with open(fileName) as self.json_data:
            self.jsonMap = json.load(self.json_data)
        #print("Json file loaded in ")

        self.nodes=[]
        for element in self.jsonMap["NOD"]:
            self.nodes.append(Node(element['NAME'].encode("utf-8"),element['OUT_BACK'].encode("utf-8"),element['OUT_RIGHT'].encode("utf-8"),element['OUT_AHEAD'].encode("utf-8"),element['OUT_LEFT'].encode("utf-8"),element['COORDINATES'],element['IN_BACK'].encode("utf-8"), element['IN_RIGHT'].encode("utf-8"), element['IN_AHEAD'].encode("utf-8"), element['IN_LEFT'].encode("utf-8")))
    
            
    def getNodes(self):
        return self.nodes
	
	
    def getNodeByCoordinates(self, x,y):
        for node in self.nodes:
            if ((node.getCoord()[0] == x) and (node.getCoord()[1] == y)):
                return node.getName()
        return 0

#testMap = Map("HARTA_OVALA_NOUA.json")	
#print(testMap.getNodes()[0].getName().decode('UTF-8'))
