import math
import copy
import ReadMap as RM

''' Clasa pentru reprezentarea nodului intr-un arbore. Contine inlcusiv un nod de harta'''

class graphNode:
    def __init__(self, node, sons):
        self.node = node
        self.sons = sons
        self.score = 0
        self.rank = 0
    
    def addSon(self, nodeSon):
        self.sons.append(nodeSon)
        
    def getSons(self):
        return self.sons
    
    def getScore(self):
        return self.score
    
    def getRank(self):
        return self.rank
    
    def getElementNode(self):
        return(self.node)
    
    def hasSons(self):
        if (len(self.sons) != 0):
            return True
        else:
            return False
    
    def addScore(self, score):
        self.score += score
        
    def addRank(self, rank):
        self.rank = rank
#------------------------------------

'''Clasa care are ca scop calcularea drumului minim dintr-un punct A in B date prin doua ibiecte de tip Node'''
class PathCalculator:
    def __init__(self, map):
        self.map = map
        self.DEEPLIMIT = 2
        
    def calculateEuclidianDistance(self,node1, node2):
        coord1 = node1.getCoord()
        coord2 = node2.getCoord()
        return (math.sqrt(math.pow((coord1[0]-coord2[0]),2)+math.pow(((coord1[1]-coord2[1])),2)))
        
    def hasNoNeighbours(self, node):
        self.nei = node.getNeibourgh()
        if (nei == [0,0,0,0]):
            return False
        else:
            return True
        
    def getNodeByName(self, name):
        if (name == 0): 
            return 0
        for nod in self.map:
            if (nod.getName() == name):
                return nod
        return 0
        
    def isFinalNode(self, node):
        if (node.getCoord() == self.finalNode.getCoord()):
            return True
        else:
            return False
        
    def isIntersection(self,node):
        self.leg = 0
        for nei in node.getNeibourgh():
            if (nei != 0):
                self.leg += 1
        if (self.leg > 1):
            return True
        else:
            return False
        
    def getTheOnlyNext(self, node):
        self.nei = node.getNeibourgh()
        for i in range(0,len(self.nei)):
            if(self.nei[i] != 0):
                return [self.nei[i], i]
        return 0
    
    def makeGScores(self,gNode, currentScore, currentRank):                                      
        currentScore += self.calculateEuclidianDistance(gNode.getElementNode(),self.finalNode)
        currentRank += 1
        gNode.addScore(self.calculateEuclidianDistance(gNode.getElementNode(),self.finalNode))
        gNode.addRank(currentRank)
        for son in gNode.getSons():
            self.makeGScores(son, currentScore, currentRank)
        
    def getGNodeByName(self, name, gNode):
        if(gNode.getElementNode().getName() == name):
            return gNode
        else:
            if (currentGNode.hasSons()):
                for son in gNode.gerSons():
                    getGNodeByName(self, name, son)
            
    def doRecursiveAdding(self, gNode, rank):
        rank+=1
        if (rank == self.DEEPLIMIT):
            return
        for gSonNodeName in gNode.getElementNode().getNeibourgh():
            self.posibleSon = self.getNodeByName(gSonNodeName)
            if (self.posibleSon != 0):
                self.addedNode = graphNode(self.posibleSon,[])
                gNode.addSon(self.addedNode)
                self.doRecursiveAdding(self.addedNode, rank)
        if(rank == 1):
            return gNode
        
        
    def createPredictiveGraph(self, node):
        self.curentN = graphNode(node,[])
        self.curentN = self.doRecursiveAdding(self.curentN, 0)
        self.makeGScores(self.curentN, 0, 0)
        return self.curentN
    
    def seePredictiveGraph(self, gNode):
        if(gNode != 0):
            print(gNode.getElementNode().getName())
            print('Cu scorul: ')
            print(gNode.getScore())
            print('Cu fii: ')
            for son in gNode.getSons():
                print(son.getElementNode().getName())
            print('---------')
            for son in gNode.getSons():
                self.seePredictiveGraph(son)
                
    def getShortestPath(self, currentPath, currentGNode, i):
        currentPath.append(currentGNode)
        i += 1 
        if (currentGNode.getElementNode().getName() != self.finalNode.getName()):
            #print("DING DING")
            self.pathToFInalNode = currentPath
        if((currentGNode.getRank() > self.maxRank) and (currentGNode.getElementNode().getName() != self.finalNode.getName())):
            self.minScore = currentGNode.getScore()
            self.maxRank = currentGNode.getRank()
            self.possiblePath = copy.deepcopy(currentPath)
            
        elif(((currentGNode.getElementNode().getName() == self.finalNode.getName()) or (currentGNode.getRank() == self.maxRank)) and (self.minScore > currentGNode.getScore())):
            self.minScore = currentGNode.getScore()
            self.maxRank = currentGNode.getRank()
            self.possiblePath = copy.deepcopy(currentPath)
        
        if (currentGNode.hasSons()):
            for son in currentGNode.getSons():
                if(son != []):
                    if self.pathToFinalNode == []:
                        for node in currentPath:
                        
                            self.getShortestPath(copy.deepcopy(currentPath), son, i)

    
    def getOptimalPath(self,nodS, nodF):
        #print('All nodes: ')
        #for nod in self.map:
        #   print(str(nod.getName()) + ' ' + str(nod.getCoord()) + ' ' + str(nod.getNeibourgh()))
        print('Start node: ' + str(nodS) + '   final node: ' + str(nodF))
        self.startNodeName = nodS
        self.finalNodeName = nodF
        self.startNode = self.getNodeByName(self.startNodeName)
        self.finalNode = self.getNodeByName(self.finalNodeName)
        self.curentNode = self.startNode
        self.optimalPath = []
        self.possiblePath = []
        self.pathToFinalNode = []
        #print('Initializari facute!')
        while(self.curentNode.getName() != self.finalNode.getName()):
            #print("AAA")
            if(self.getTheOnlyNext(self.curentNode) == 0):
                print('ERROR: Inexistent path!')
                quit()
        #   print('Comparare esuata: ' + str(self.curentNode.getName()) + '!=' + str(self.finalNode.getName()))
            if(self.isIntersection(self.curentNode) == False):
                                self.type = 0
                                self.nextElement = self.getTheOnlyNext(self.curentNode)
                                for nei in self.curentNode.getNeibourgh():
                                        self.type += 1
                                        if (nei != 0):
                                                break
                #print('Efectueaza instructiuni nu e intersectie')
                #print('Posibilitati deplasare ' + str(self.curentNode.getNeibourgh()) + 'ALEGERE: ' + str(self.type))
                                self.optimalPath.append([self.curentNode.getName(), self.type])
                                self.curentNode = self.getNodeByName(self.nextElement[0])
                
            else:
                #if(self.curentNode.getName() == "NOD12"):
                #    print('DA')
                #print(self.curentNode.getNeibourgh())
                #print('Efectueaza instructiuni conditie INTERSECTIE')
                self.predictiveGraph = self.createPredictiveGraph(self.curentNode)
                #print('Creaza arbore predictii.')
                #self.seePredictiveGraph(self.predictiveGraph)
                self.maxRank = 0
                self.minScore = self.predictiveGraph.getScore()
                self.pathToFInalNode = []
                self.possiblePath = []
                #print('Initializeaza maxRanl, minScore, possiblePath')
                #print('Incepere efectuare predictii.')
                self.getShortestPath([],self.predictiveGraph, 0)
                if(self.pathToFinalNode != []):
                    self.possiblePath = self.pathToFinalNode
                #print('Finalizare predictii.')
                #for element in self.possiblePath:
                #    print(element.getElementNode().getName(),"peace ")
                try:    
                    self.curentNode = self.possiblePath[0].getElementNode()
                    self.nextNode = self.possiblePath[1].getElementNode()
                    if(self.curentNode.getNeibourgh()[0] == self.nextNode.getName()):
                        #print('1')
                        self.optimalPath.append([self.curentNode.getName(), 1])
                    elif(self.curentNode.getNeibourgh()[1] == self.nextNode.getName()):
                        #print('2')
                        self.optimalPath.append([self.curentNode.getName(), 2])
                    elif(self.curentNode.getNeibourgh()[2] == self.nextNode.getName()):
                        #print('3')
                        self.optimalPath.append([self.curentNode.getName(), 3])
                    elif(self.curentNode.getNeibourgh()[3] == self.nextNode.getName()):
                        #print('4')
                        self.optimalPath.append([self.curentNode.getName(), 4])
                    print(self.optimalPath)                        
                    self.curentNode = self.possiblePath[1].getElementNode()
                    
                except:
                    print('Exceptie path')
                    self.neiTemp = self.possiblePath[0].getElementNode().getNeibourgh()
                    for i in range(0,4):
                        if(self.neiTemp[i] == self.finalNode.getName()):
                            self.optimalPath.append([self.possiblePath[0].getElementNode().getName(), i+1])
                            print('Adaug '+str(self.possiblePath[0].getElementNode().getName())+" "+str(i+1))
                            self.curentNode = self.finalNode                   
        
        #print('Comparare reusita: ' + str(self.curentNode.getName()) + '!=' + str(self.finalNode.getName()))
        self.optimalPath = self.interpretatePath(self.optimalPath)
        self.optimalPath = self.linkLogicalPath(self.optimalPath)
        print(self.optimalPath)
        return self.optimalPath

    def linkLogicalPath(self, finalPath):
        for i in range(1, len(finalPath) - 1):
            self.nod0 = self.getNodeByName(finalPath[i-1][0])
            self.nod1 = self.getNodeByName(finalPath[i][0])
            self.nod2 = self.getNodeByName(finalPath[i+1][0])
            print(self.nod1.getName())
            print(self.nod2.getName())
            self.inNei1 = self.nod1.getInNeibourgh()
            print(self.inNei1)
            self.nod1Coords = self.nod1.getCoord()
            self.nod2Coords = self.nod2.getCoord()

            if self.nod2Coords[1] == self.nod1Coords[1]:

                if self.inNei1[0] == self.nod0.getName(): #IN_LEFT
                    print("G1")
                    if(self.nod2Coords[1] > self.nod1Coords[1]):
                        finalPath[i][1] = "LEFT"
                    elif(self.nod2Coords[1] < self.nod1Coords[1]):
                        finalPath[i][1] = "RIGHT"
                    else:
                        finalPath[i][1] = "FORWARD"

                if self.inNei1[1] == self.nod0.getName(): #IN_RIGHT
                    print("G2")
                    if(self.nod2Coords[1] < self.nod1Coords[1]):
                        finalPath[i][1] = "LEFT"
                    elif(self.nod2Coords[1] > self.nod1Coords[1]):
                        finalPath[i][1] = "RIGHT"
                    else:
                        finalPath[i][1] = "FORWARD"

            
            if self.nod2Coords[1] == self.nod1Coords[1]:

                if self.inNei1[2] == self.nod0.getName(): #IN_BACK
                    print("G3")
                    if(self.nod2Coords[0] > self.nod1Coords[0]):
                        finalPath[i][1] = "LEFT"
                    elif(self.nod2Coords[0] < self.nod1Coords[0]):
                        finalPath[i][1] = "RIGHT"
                    else:
                        finalPath[i][1] = "FORWARD"
                
                if self.inNei1[3] == self.nod0.getName(): #IN_FORWARD
                    print("G4")
                    if(self.nod2Coords[0] < self.nod1Coords[0]):
                        finalPath[i][1] = "LEFT"
                    elif(self.nod2Coords[0] > self.nod1Coords[0]):
                        finalPath[i][1] = "RIGHT"
                    else:
                        finalPath[i][1] = "FORWARD"

        return finalPath
            
    def interpretatePath(self,path):
        print(len(path))
        print(path)
        #for i in range(0,len(path)-1):
        #   path[i][1] = path[i+1][1]
        
        for self.tuple in path:
            if(self.tuple[1] == 1):
                self.tuple[1] = 'BACK'
                print("ALERT: May be a problem. 'Back' in path");
            elif(self.tuple[1] == 2):
                self.tuple[1] = 'RIGHT'
            elif(self.tuple[1] == 3):
                self.tuple[1] = "FORWARD"
            else:
                self.tuple[1] = "LEFT"
        
        #self.n = len(path) - 1
        #for i in range(0,self.n - 1):
        #   print(str(i), ' i+1= '+str(i+1)+' n = ' + str(self.n))
        #   if(i < self.n):
        #       if(path[i][0] == path[i+1][0]):
        #           del(path[i])
        #           self.n += -1
        
        
        if (path[len(path)-1][0] != self.finalNode.getName()):
            path.append([self.finalNode.getName(), "STOP"])
        else:
            path[len(path)-1][1] = "STOP"
        
        #print('Pathfinding done!')
        return path
#-----------------------------------
#map = [
#       Node('NOD0',[0,0,'NOD1',0],[7,7]), #BRAL
#       Node('NOD1',[0,0,'NOD2',0],[6,7]),
#       Node('NOD2',[0,0,'NOD3',0],[5,7]),
#       Node('NOD3',[0,0,'NOD4',0],[5,6]),
#       Node('NOD4',[0,0,'NOD5',0],[5,5]),
#       Node('NOD5',[0,'NOD7','NOD6',0],[5,4]),
#       Node('NOD6',[0,0,'NOD8','NOD10'],[5,3]),
#       Node('NOD7',[0,0,0,0],[6,4]),
#       Node('NOD8',[0,0,'NOD9',0],[5,2]),
#       Node('NOD9',[0,0,0,0],[5,1]),
#       Node('NOD10',[0,0,'NOD11',0],[4,3]),
#       Node('NOD11',[0,0,'NOD12',0],[3,3]),
#       Node('NOD12',[0,0,'NOD13',0],[2,3]),
#       Node('NOD13',[0,0,0,0],[1,3])

#]

#AA SE INCARCA HARTA
#map = RM.Map('MyIntersectionMap.json')
#pathCalculator calculeaza drumul minim
#pathCalculator = PathCalculator(map.getNodes())

#p va fi drumul minim. Apelul metodei presupune apelul cu nummele nodurilor. map.getNodeByCoordinates(x,y) va returna numele nodului cu coordonatele x,y
#P = pathCalculator.getOptimalPath(map.getNodeByCoordinates(0.9,0), map.getNodeByCoordinates(2.25,0.9))
#asa arata p dupa calcularea drumului minim
#for nod in P:
#        print(nod)
#PS: DACA GASITI ORICE BUG SAU NU INTELEGETI CEVA, SA NE SPUNETI! :)) SPOR!
