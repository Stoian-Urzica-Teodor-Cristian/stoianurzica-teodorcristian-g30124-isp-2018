import PathCalculator2 as PC
#import CarClient as CC
import ReadMap as RM
import math
import copy
import time
from random import randint
from threading import Thread

class Localization(Thread):

        FLAG_FORWARD = 0
        FLAG_RIGHT = 2
        FLAG_LEFT = 1
        FLAG_STOP = 3
        
        def __init__(self, threadID, name):
                Thread.__init__(self)
                self.threadID = threadID
                self.divisionNumber = 0
                self.CONST_PARKING_MINIMUM_DISTANCE = 2 # DE AJUSTAT
                self.name = name
                self.speed = 0.2# DE VAZUT IN CE UNITATE DE MASURA
                self.REFERENCE_SPEED = 2
                self.REFERENCE_DISTANCE = 1
                self.REFERENCE_ENCODER_SPEED = 20
                self.raza1 = 0
                self.raza2 = 0
                self.flag = 0
                self.delta_t = 0
                self.delta_r = 0
                self.ORIGIN = [0,0]
                self.current_index = 0
                self.next_index = 1
                self.map = RM.Map('SmallIntersection.json')
                #self.parkingNode = self.getParkNode() # DA 0 CA NU ESTE PARKING NODE
                self.pathCalculator = PC.PathCalculator(self.map.getNodes())
                #self.path = self.pathCalculator.getOptimalPath(self.map.getNodeByCoordinates(0 ,0.9), self.map.getNodeByCoordinates(0.9, 0))
                #self.path = self.pathCalculator.getOptimalPath(self.map.getNodeByCoordinates(2.25,0.9), self.map.getNodeByCoordinates(0.9,0))
                self.path = self.pathCalculator.getOptimalPath("NOD2","NOD13")
                print('Current path: ' + str(self.path))
                #print('Parking node: ' + str(self.parkingNode.getName()))
                self.current_position = self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord() # TREBUIE INIT CU CE VINE DE LA GPS
                self.next_position = self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()
                self.current_time = time.time() % 60
                self.GPS_TIME = 5
                self.GPS_PREV_TIME = 0
                self.PREV_TIME = 0
                #self.gpsPosition = [CC.car_poz.real/10, CC.car_poz.imag/10]
                self.gpsPosition = [0,0]
                #print(self.gpsPosition)

        def getAproximatedCoordinate(self, val):
                val = val / 0.45
                val = math.trunc(val)
                val = val * 0.45
                print(val)

        #PARKING NODE FUNCTION
        def getParkNode(self):
                for node in self.map.getNodes():
                        print(node.getNeibourgh())
                        if ((node.getNeibourgh()[0] == 0) and (node.getNeibourgh()[1] == 0) and (node.getNeibourgh()[2] == 0) and (node.getNeibourgh()[3] == 0)):
                                return node
                print('No praking node!')
                return 0

        #PARKIGN CLOSE CHECK
        def checkIsCloseToPark():
                if (self.pathCalculator.calculateEuclidianDistance(self.map.getNodeByCoordinates(self.position[0], self.position[1]), self.map.getNodeByCoordinates(self.parkingNode.getCoord()[0],self.parkingNode.getCoord()[1])) < self.CONST_PARKING_MINIMUM_DISTANCE):
                        return True
                return False

        def dividePath(self):
                pass
                
        def findPathTo(self, x, y):
                self.path = self.pathCalculator.getOptimalPath(self.map.getNodeByCoordinates(self.curent_position[0], self.curent_position[1]), self.map.getNodeByCoordinates(x,y))

        def reclaculate_path(self, x1,y1,x2,y2):
                self.path = self.pathCalculator.getOptimalPath(self.map.getNodeByCoordinates(x1,y1), self.map.getNodeByCoordinates(x2,y2))
                
        def calculate_delta_r(self,point_1,point_2):
                self.raza1=math.sqrt(math.pow(self.ORIGIN[0]-point_1[0],2)+math.pow(self.ORIGIN[1]-point_1[1],2))
                self.raza2=math.sqrt(math.pow(self.ORIGIN[0]-point_2[0],2)+math.pow(self.ORIGIN[1]-point_2[1],2))
                return math.fabs(self.raza1 - self.raza2)

        def isTimeToChange(self,t):
                if (t - self.PREV_TIME >= self.delta_t):
                        self.PREV_TIME = t
                        print('Conditie schimbare indeplinita')
                        return True
                return False

        def changeSpeed(self):
                a = randint(0,10)
                if(a<=5):
                        self.speed += 0.2
                else:
                        self.speed -= 0.2

        #DE LUCRAT AICI
        def isTimeForGPSChange(self,t):
                #self.gpsPosition = gps.getPosition()
                if (t - self.GPS_PREV_TIME >= self.GPS_TIME):
                        self.GPS_PREV_TIME = t
                        self.distanceToCurrentPoint = math.sqrt(math.pow(self.gpsPosition[0]-self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()[0],2) +
                                                                math.pow(self.gpsPosition[1]-self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()[1],2))

                        self.distanceToNextPoint = math.sqrt(math.pow(self.gpsPosition[0]-self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()[0],2) +
                                                             math.pow(self.gpsPosition[1]-self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()[1],2))

                        if(self.distanceToCurrentPoint < self.distanceToNextPoint):
                                pass
                        else:
                                print('GPS a intrat la timp curent = ' + str(self.current_time))
                                self.current_index += 1
                                self.next_index +=1
                                self.current_time = t
                                self.current_position = self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()
                                self.next_position = self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()
                        return True
                else:
                        return False

        def changePosition(self,t):
                if(self.isTimeToChange(t)):
                        print('Change position ')
                        t = time.time() % 60
                        self.current_time = t % 60
                        self.current_index +=1
                        self.next_index += 1
                        print('S-a modificat pozitia la timp curent = ' + str(t % 60) + 'la index = ' + str(self.current_index));
                        try:
                                self.current_position = self.pathCalculator.getNodeByName(self.path[self.current_index][0]).getCoord()
                                self.next_position = self.pathCalculator.getNodeByName(self.path[self.next_index][0]).getCoord()
                        except:
                                print('Nu mai calculez indicii.')
                                quit()

        def isEndOfTheRoad(self):
                if(self.next_index == len(self.path)):
                        return True
                else:
                        return False

        def getRoadType(self):
                try:
                        self.nodeFromPath = self.pathCalculator.getNodeByName(self.path[self.current_index][0])
                        if(self.path[self.current_index][1] == 'STOP'):
                                self.flag = 3
                                return self.flag
                
                        if(self.path[self.current_index][1] == 'LEFT'):
                                self.flag = 1

                        if(self.path[self.current_index][1] == 'RIGHT'):
                                self.flag = 2
                    
                        if(self.path[self.current_index][1] == 'FORWARD'):
                                self.nei = self.path[self.current_index][2].getNeibourgh()
                                if(self.nei[1]!=0 or self.nei[3]!=0):
                                        self.flag = 4 #INTERSECTIE DAR FORWARD
                                else:
                                        self.flag = 0
                    
                        return self.flag
                except:
                        print("Nu exista nod urmator")
                return -1


        def getNextRoadType(self):
                try:
                        self.nodeFromPath = self.pathCalculator.getNodeByName(self.path[self.current_index][0])
                        if(self.path[self.next_index][1] == 'STOP'):
                                self.flag = 3
                                return self.flag
                
                        if(self.path[self.next_index][1] == 'LEFT'):
                                self.flag = 1

                        if(self.path[self.next_index][1] == 'RIGHT'):
                                self.flag = 2
                    
                        if(self.path[self.next_index][1] == 'FORWARD'):
                                self.nei = self.path[self.next_index][2].getNeibourgh()
                                if(self.nei[1]!=0 or self.nei[3]!=0):
                                        self.flag = 4 #INTERSECTIE DAR FORWARD
                                else:
                                        self.flag = 0
                    
                        return self.flag
                except:
                        print("Nu exista nod urmator")
                return -1

        def getNextNextRoadType(self):
                try:
                        self.nodeFromPath = self.pathCalculator.getNodeByName(self.path[self.current_index][0])
                        if(self.path[self.next_index+1][1] == 'STOP'):
                                self.flag = 3
                                return self.flag
                
                        if(self.path[self.next_index+1][1] == 'LEFT'):
                                self.flag = 1

                        if(self.path[self.next_index+1][1] == 'RIGHT'):
                                self.flag = 2
                    
                        if(self.path[self.next_index+1][1] == 'FORWARD'):
                                self.nei = self.path[self.next_index+1][2].getNeibourgh()
                                if(self.nei[1]!=0 or self.nei[3]!=0):
                                        self.flag = 4 #INTERSECTIE DAR FORWARD
                                else:
                                        self.flag = 0
                    
                        return self.flag
                except:
                        print("Nu exista nod urmator")
                return -1

        def getNextNextNextRoadType(self):
                try:
                        self.nodeFromPath = self.pathCalculator.getNodeByName(self.path[self.current_index][0])
                        if(self.path[self.next_index+2][1] == 'STOP'):
                                self.flag = 3
                                return self.flag
                
                        if(self.path[self.next_index+2][1] == 'LEFT'):
                                self.flag = 1

                        if(self.path[self.next_index+2][1] == 'RIGHT'):
                                self.flag = 2
                    
                        if(self.path[self.next_index+2][1] == 'FORWARD'):
                                self.nei = self.path[self.next_index+2][2].getNeibourgh()
                                if(self.nei[1]!=0 or self.nei[3]!=0):
                                        self.flag = 4 #INTERSECTIE DAR FORWARD
                                else:
                                        self.flag = 0
                    
                        return self.flag
                except:
                        print("Nu exista nod urmator")
                return -1

        def getNextNextNextRoadType(self):
                try:
                        self.nodeFromPath = self.pathCalculator.getNodeByName(self.path[self.current_index][0])
                        if(self.path[self.next_index+2][1] == 'STOP'):
                                self.flag = 3
                                return self.flag
                
                        if(self.path[self.next_index+2][1] == 'LEFT'):
                                self.flag = 1

                        if(self.path[self.next_index+2][1] == 'RIGHT'):
                                self.flag = 2
                    
                        if(self.path[self.next_index+2][1] == 'FORWARD'):
                                self.nei = self.path[self.next_index+2][2].getNeibourgh()
                                if(self.nei[1]!=0 or self.nei[3]!=0):
                                        self.flag = 4 #INTERSECTIE DAR FORWARD
                                else:
                                        self.flag = 0
                    
                        return self.flag
                except:
                        print("Nu exista nod urmator")
                return -1
        

        #PE BUTUCI TREBUIE DATE DIN TESTE
        def refreshSpeed(self, encSpeed):
                return encSpeed * self.REFERENCE_SPEED / self.REFERENCE_ENCODER_SPEED

        def run(self):
                pass
                print(self.getNextNextRoadType())
                '''#self.t1 = time.time()
                print(self.getAproximatedCoordinate(2.26))
                while True:
                        while (self.isEndOfTheRoad() == False):
                                #time.sleep(0.1)
                                #self.speed = self.refreshSpeed(2) ##AICI VALOAREA ENCODERULUI DE PUS
                                self.delta_r = self.calculate_delta_r(self.current_position, self.next_position)
                                #print('DeltaR = ' + str(self.delta_r))
                                self.delta_t = self.delta_r / self.speed
                                #print('DeltaT =  ' + str(self.delta_t))
                                if(self.isTimeForGPSChange(time.time() % 60) == False):
                                        #print('RECALCULATION POSITIONS')
                                        if(self.speed!=0):
                                                self.changePosition(time.time() % 60)
                                        else:
                                                pass # CRESC DELAYURILE
                                else:
                                        print('GPS')
                                #self.t2 = time.time()-self.t1
                                #self.t1 = time.time()
                                #print('Timp in secunde: ' + str(self.t2 / 60))'''

##TESTE##

print("#############################################")
print("###########    NEW TEST      ################")
print("#############################################")
L = Localization('1', 'LocalizationThread')
time.sleep(1)
L.start()
#print('TYPE: ' + str(L.getRoadType()))
#L.run()
