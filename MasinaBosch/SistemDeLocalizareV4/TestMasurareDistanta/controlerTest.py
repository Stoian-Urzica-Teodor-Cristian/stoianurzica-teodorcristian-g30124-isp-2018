import SerialHandler as SH
import threading,serial,time,sys
import SaveEncoder
import cv2

serialHandler = SH.SerialHandler()


def main():
    
    pwm=0.0
    e=SaveEncoder.SaveEncoder("EncoderPID%.2f"%pwm+".csv")
    e.open()
    
    ev1=threading.Event()
    ev2=threading.Event()
    serialHandler.readThread.addWaiter("MCTL",ev1,e.save)
    serialHandler.readThread.addWaiter("BRAK",ev1,e.save)
    serialHandler.readThread.addWaiter("ENPB",ev2,e.save)
    while (5>2):
        key = cv2.waitKey(1) &0xFF
        angle = 0
        
        if key == ord("w"):
            pwm = 10
            print("ENTER")
        if key == ord("a"):
            angle = 25
            
            print("ENTER")
        if key == ord("d"):
            angle = -25
            
            print("ENTER")
        if key == ord("s"):
            pwm = 0.0
            
            print("ENTER")
        serialHandler.sendBrake(0.0)
        
        serialHandler.sendMove(pwm,angle)
main()
