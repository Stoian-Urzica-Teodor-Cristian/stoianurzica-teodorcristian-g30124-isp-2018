import json;
	
class Map:
	
	def __init__(self, fileName):
		self.optimalPath = []
		self.matrixMap =[]
		self.matrixDimension = 0
		self.jsonMap = []
		self.matrixDimensionX=0
		self.matrixDimensionY=0
		
		with open(fileName) as self.json_data:
			self.jsonMap = json.load(self.json_data)
		print("Json file loaded in ")
		
		
	def showJsonData(self):
		print(type(self.jsonMap["NOD"][0]["COORDINATES"]))
		
	def JsonMatrixDimension(self):
		self.auxminX=self.jsonMap["NOD"][0]["COORDINATES"][0]
		self.auxmaxX=self.jsonMap["NOD"][0]["COORDINATES"][0]
		self.auxminY=self.jsonMap["NOD"][0]["COORDINATES"][1]
		self.auxmaxY=self.jsonMap["NOD"][0]["COORDINATES"][1]
		print(len(self.jsonMap["NOD"]))
		for nod in self.jsonMap["NOD"]:
			
			if (self.auxminX>nod["COORDINATES"][0]):
				self.auxminX=nod["COORDINATES"][0]
				
			if (self.auxmaxX<nod["COORDINATES"][0]):
				self.auxmaxX=nod["COORDINATES"][0]
				
			self.matrixDimensionX=int((abs(self.auxmaxX)+abs(self.auxminX))/0.45)
			
			if (self.auxminY>nod["COORDINATES"][1]):
				self.auxminY=nod["COORDINATES"][1]
				
			if (self.auxmaxY<nod["COORDINATES"][1]):
				self.auxmaxY=nod["COORDINATES"][1]
				
			self.matrixDimensionY=int((abs(self.auxmaxY)+abs(self.auxminY))/0.45)
			
		print("Min X:" + str(self.auxminX))
		print("Max X:" + str(self.auxmaxX))
		print("Min Y:" + str(self.auxminY))
		print("Max Y:" + str(self.auxmaxY))
	
		
	def makeJsonToMatrix(self):
		self.JsonMatrixDimension()
		print(str(self.matrixDimensionX)+" "+str(self.matrixDimensionY))
		
		self.matrixMap = [[0 for x in range(self.matrixDimensionX)] for y in range(self.matrixDimensionY)]
		
		#for nod in self.jsonMap["NOD"]:
		#	self.matrixMap[int(self.matrixDimensionX-(nod["COORDINATES"][0]/0.45))][int(self.matrixDimensionY+(nod["COORDINATES"][1]/0.45))]
		self.i = 0
		for nod in self.jsonMap["NOD"]:
			self.matrixMap[int(nod["COORDINATES"][0]+abs(self.auxminX)/0.45)][int(nod["COORDINATES"][1]+abs(self.auxminY))] = 1
			self.i+=1;
		
		print(self.i)
		
		for linie in self.matrixMap:
			print(linie)
			
		self.nrDubluri = -1;
		for nod in self.jsonMap["NOD"]:
			for nod2 in self.jsonMap["NOD"]:
				if((nod2["COORDINATES"][0] == nod["COORDINATES"][0]) and (nod2["COORDINATES"][1] == nod["COORDINATES"][1])):
				   self.nrDubluri += 1
			print("Nr dubluri: "+str(self.nrDubluri))
			self.nrDubluri = -1
		
		
testMap = Map("Map.json")
testMap.showJsonData()
testMap.makeJsonToMatrix()

	