import math
import copy

class Node:
	def __init__(self, name, neibourgh, coordinates):
		self.coordinates = coordinates
		self.name = name
		self.neibourgh = neibourgh
		self.DEEPLIMIT = 6
		
	def getName(self):
		return self.name
	
	def getCoord(self):
		return self.coordinates
	
	def getNeibourgh(self):
		return self.neibourgh
	
	def getNeibourghByPosition(self, pos):
		return self.neibourgh[pos]
	
	def __del__(self):
		del(self.coordinates)
		del(self.name)
		del(self.neibourgh)
		
#------------------------------------

class graphNode:
	def __init__(self, node, sons):
		self.node = node
		self.sons = sons
		self.score = 0
		self.rank = 0
	
	def addSon(self, nodeSon):
		#print("Son adaugat: " + nodeSon.getElementNode().getName())
		self.sons.append(nodeSon)
		
	def getSons(self):
		return self.sons
	
	def getScore(self):
		return self.score
	
	def getRank(self):
		return self.rank
	
	def getElementNode(self):
		return(self.node)
	
	def hasSons(self):
		if (len(self.sons) != 0):
			return True
		else:
			return False
	
	def addScore(self, score):
		self.score += score
		
	def addRank(self, rank):
		self.rank = rank
#------------------------------------

class PathCalculator:
	def __init__(self, map):
		self.map = map
		self.startNode = map[0]
		self.finalNode = map[len(map)-1]
		self.DEEPLIMIT = 6
		
	def calculateEuclidianDistance(self,node1, node2):
		coord1 = node1.getCoord()
		coord2 = node2.getCoord()
		#print(node1.getCoord())
		#print(node2.getCoord())
		#print(math.sqrt(math.pow((coord1[0]-coord2[0]),2)+math.pow((coord1[1]-coord2[1]),2)))
		return (math.sqrt(math.pow((coord1[0]-coord2[0]),2)+math.pow(((coord1[1]-coord2[1])),2)))
		
	def hasNoNeighbours(slef, node):
		self.nei = node.getNeibourgh()
		if (nei == [0,0,0,0]):
			return False
		else:
			return True
		
	def getNodeByName(self, name):
		if (name == 0): 
			return 0
		for nod in map:
			if (nod.getName() == name):
				return nod
		return 0
		
	def isFinalNode(self, node):
		if (node.getCoord() == self.finalNode.getCoord()):
			return True
		else:
			return False
		
	def isIntersection(self,node):
		self.leg = 0
		for nei in node.getNeibourgh():
			if (nei != 0):
				self.leg += 1
		if (self.leg > 1):
			return True
		else:
			return False
		
	def getTheOnlyNext(self, node):
		self.nei = node.getNeibourgh()
		for i in range(0,len(self.nei)):
			if(self.nei[i] != 0):
				break
		return [self.nei[i], i]
	
	def makeGScores(self,gNode, currentScore, currentRank):                                        #NU FACE BINE SCORUL
		currentScore += self.calculateEuclidianDistance(gNode.getElementNode(),self.finalNode)
		currentRank += 1
		gNode.addScore(self.calculateEuclidianDistance(gNode.getElementNode(),self.finalNode))
		gNode.addRank(currentRank)
		#print('Calculez pentru ' + str(gNode.getElementNode().getName()))
		for son in gNode.getSons():
			self.makeGScores(son, currentScore, currentRank)
		
	def getGNodeByName(self, name, gNode):
		if(gNode.getElementNode().getName() == name):
			return gNode
		else:
			if (currentGNode.hasSons()):
				for son in gNode.gerSons():
					getGNodeByName(self, name, son)
			
	def doRecursiveAdding(self, gNode, rank):
		rank+=1
		if (rank == self.DEEPLIMIT):
			return
		#print(gNode.getElementNode().getName())
		for gSonNodeName in gNode.getElementNode().getNeibourgh():
			self.posibleSon = self.getNodeByName(gSonNodeName)
			if (self.posibleSon != 0):
				self.addedNode = graphNode(self.posibleSon,[])
				gNode.addSon(self.addedNode)
				self.doRecursiveAdding(self.addedNode, rank)
		if(rank == 1):
		#	print('Returnez ' + str(gNode.getElementNode().getName()))
			return gNode
		
		
	def createPredictiveGraph(self, node):
		self.curentN = graphNode(node,[])
		self.curentN = self.doRecursiveAdding(self.curentN, 0)
		#print(self.curentN)
		self.makeGScores(self.curentN, 0, 0)
		return self.curentN
	
	def seePredictiveGraph(self, gNode):
		if(gNode != 0):
			print(gNode.getElementNode().getName())
			print('Cu scorul: ')
			print(gNode.getScore())
			print('Cu fii: ')
			for son in gNode.getSons():
				print(son.getElementNode().getName())
			print('---------')
			for son in gNode.getSons():
				self.seePredictiveGraph(son)
				
	def getShortestPath(self, currentPath, currentGNode, i):
		currentPath.append(currentGNode)
		i += 1 
		if((currentGNode.getRank() > self.maxRank) and (currentGNode.getElementNode().getName() != self.finalNode.getName())):
			self.minScore = currentGNode.getScore()
			self.maxRank = currentGNode.getRank()
			self.possiblePath = copy.deepcopy(currentPath)
			
		elif(((currentGNode.getElementNode().getName() == self.finalNode.getName()) or (currentGNode.getRank() == self.maxRank)) and (self.minScore < currentGNode.getScore())):
			self.minScore = currentGNode.getScore()
			self.possiblePath = copy.deepcopy(currentPath)
		
		if (currentGNode.hasSons()):
			for son in currentGNode.getSons():
				if(son != []):
					self.getShortestPath(copy.deepcopy(currentPath), son, i)

	
	def getOptimalPath(self):
		self.curentNode = self.startNode
		self.optimalPath = []
		self.possiblePath = []
		while(self.curentNode.getName() != self.finalNode.getName()):
			if(self.isIntersection(self.curentNode) == False):
				#print("Ciclu 1")
				self.nextElement = self.getTheOnlyNext(self.curentNode)
				self.optimalPath.append([self.nextElement[0], 3])
				self.curentNode = self.getNodeByName(self.nextElement[0])
				#print(self.nextElement[0])
	
			else:
				#print('Ciclu 2')
				#print('Fac predictii in nodul ' + str(self.curentNode.getName()))
				self.predictiveGraph = self.createPredictiveGraph(self.curentNode)
				#self.seePredictiveGraph(self.predictiveGraph)
				self.maxRank = 0
				self.minScore = self.predictiveGraph.getScore()
				self.possiblePath = []
				self.getShortestPath([],self.predictiveGraph, 0)
				#self.seePredictiveGraph(self.predictiveGraph)
				#for gNod in self.possiblePath:
				#	print(gNod.getElementNode().getName())
				try:
					self.curentNode = self.possiblePath[1].getElementNode()
					self.nextNode = self.possiblePath[2].getElementNode()
				except:
					print('Prea putine elemente in possiblePath')
				
				for self.i in range(0,4):
					if(self.nextNode.getName() == self.curentNode.getNeibourghByPosition(self.i)):
						break
				self.optimalPath.append([self.curentNode.getName(), self.i + 1])
				#print(self.curentNode.getName())

				#print('Prea putine elemente in explorare')
				#self.optimalPath.append([self.curentNode.getName(), 0])
				#self.optimalPath.append([self.curentNode.getName(), 0])
					
		
		#print('PATH');
		#for gNod in self.possiblePath:
		#	print(gNod.getElementNode().getName())
		self.optimalPath = self.interpretatePath(self.optimalPath)
		return self.optimalPath
			
	def interpretatePath(self,path):
		for self.tuple in path:
			if(self.tuple[1] == 1):
				self.tuple[1] = 'BACK'
				print("ALERT: May be a problem. 'Back' in path");
			elif(self.tuple[1] == 2):
				self.tuple[1] = 'RIGHT'
			elif(self.tuple[1] == 3):
				self.tuple[1] = "FORWARD"
			else:
				self.tuple[1] = "LEFT"
			
		return path
#-----------------------------------
map = [
		Node('NOD0',[0,0,'NOD1',0],[7,7]), #BRAL
		Node('NOD1',[0,0,'NOD2',0],[6,7]),
		Node('NOD2',[0,0,'NOD3',0],[5,7]),
		Node('NOD3',[0,0,'NOD4',0],[5,6]),
		Node('NOD4',[0,0,'NOD5',0],[5,5]),
		Node('NOD5',[0,'NOD7','NOD6',0],[5,4]),
		Node('NOD6',[0,0,'NOD8','NOD10'],[5,3]),
		Node('NOD7',[0,0,0,0],[6,4]),
		Node('NOD8',[0,0,'NOD9',0],[5,2]),
		Node('NOD9',[0,0,0,0],[5,1]),
		Node('NOD10',[0,0,'NOD11',0],[4,3]),
		Node('NOD11',[0,0,'NOD12',0],[3,3]),
		Node('NOD12',[0,0,'NOD13',0],[2,3]),
		Node('NOD13',[0,0,0,0],[1,3])
]

pathCalculator = PathCalculator(map)
print(pathCalculator.getOptimalPath())