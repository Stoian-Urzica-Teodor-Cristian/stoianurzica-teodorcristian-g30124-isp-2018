package g31024.StoianUrzica.TeodorCristian.l2.e7;

import java.util.Random;
import java.util.Scanner;

public class Game {
    private final String[] MESSAGES = {"Welcome gamer!","Enter a number: ","Wrong answer, your number is too high.!", "Wrong answer, your number is too low.", "YOU WIN!",
                                        "You lost! Try Againg!", "Lifes: "};
    private int lifes;
    private int gNumber, userNumber;

    public Game(){
        Random randomGen = new Random();
        gNumber = randomGen.nextInt(15);
        lifes = 3;
    }

    public void gameReset(){
        Random randomGen = new Random();
        gNumber = randomGen.nextInt(15);
        lifes = 3;
    }

    public int getLifes(){
        return lifes;
    }

    public void takeLife(){
        lifes--;
    }

    public void getUsersNumber(){
        Scanner sc = new Scanner(System.in);
        userNumber = sc.nextInt();
    }

    public String getMessage(int index){
        return MESSAGES[index];
    }

    public Boolean isWin(){
        if(userNumber == gNumber) return true;
        else return false;
    }

    public Boolean isLose(){
        if(lifes == 0) return true;
        else return false;
    }

    public Boolean userNumberIsBigger(){
        if (userNumber > gNumber) return true;
        else return false;
    }

    public Boolean userNumberIsLess(){
        if (userNumber < gNumber) return true;
        else return false;
    }
}
