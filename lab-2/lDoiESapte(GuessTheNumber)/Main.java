package g31024.StoianUrzica.TeodorCristian.l2.e7;

public class Main {

    public static void main(String[] args) {
        Game theGame = new Game();
        System.out.println(theGame.getMessage(0));
        while(!theGame.isWin()) {
            System.out.print(theGame.getMessage(1));
            theGame.getUsersNumber();
            if(!theGame.isWin()){
                theGame.takeLife();
                if(!theGame.isLose()) {
                    if (theGame.userNumberIsBigger()) System.out.println(theGame.getMessage(2));
                    if (theGame.userNumberIsLess()) System.out.println(theGame.getMessage(3));
                }
                else{
                    System.out.println(theGame.getMessage(5));
                    theGame.gameReset();
                }
            }
        }
        System.out.println(theGame.getMessage(4));
    }
}
