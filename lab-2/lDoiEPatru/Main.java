package g31024.StoianUrzica.TeodorCristian.l2.e4;

import java.util.*;

public class Main {
    private static final int NMAX = 200;

    public static void main(String[] args) {
        int n = 0;
	    int i = 0;
	    float max = 0;
	    float[] vector = new float[NMAX];
	    Scanner sc = new Scanner(System.in);
        System.out.print("Number of elements: ");
        n = sc.nextInt();
        for(i = 0; i < n; i++){
            System.out.print("Element["+ i +"]: ");
            vector[i] = sc.nextInt();
        }
        if(n>0){
            max = vector[0];
            for (i = 1; i<n; i++)
                if(vector[i]>max)
                    max = vector[i];
            System.out.println("Maximum is: " + max);
        }
        else{
            System.out.println("Vector has 0 elements.");
        }
    }
}
