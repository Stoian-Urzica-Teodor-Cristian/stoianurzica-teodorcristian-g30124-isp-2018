package g31024.StoianUrzica.TeodorCristian.l2.e5;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int n = 10;
        int i;
        int[] vector = new int[n];
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        for(i = 0; i < n; i++)
            vector[i] = rand.nextInt(1000);

        System.out.print("Initial random array: ");
        for(i = 0; i < n; i++){
            System.out.print(vector[i]+", ");
        }
        System.out.println();

        boolean ok = false;
        while(ok == false){
            ok = true;
            for(i = 0; i < n - 1; i++)
                if(vector[i] > vector[i+1]){
                    int aux = vector[i];
                    vector[i] = vector[i+1];
                    vector[i+1] = aux;
                    ok = false;
                }
        }
        System.out.print("Sorted random array: ");
        for(i = 0; i < n; i++){
            System.out.print(vector[i]+", ");
        }
    }
}
