package g31024.StoianUrzica.TeodorCristian.l2.e6;

import java.util.Scanner;

public class Main {

    //Recursive method
    public static int doRecursiveFactorial(int i, int n, int res ){
        if (i>=n) return res*i;
        else
        return doRecursiveFactorial(i+1, n, res*i);
    }

    public static int doRecursiveFactorial(int n){
        if(n==0) return 1;
        else
        return doRecursiveFactorial(2, n, 1);
    }

    //Iterative method
    public static int doSimpleFactorial(int n){
        int res = 1;
        if(n <= 1) return 1;
        else{
            int i;
            for(i = 2; i <= n; i++){
                res *= i;
            }
            return res;
        }
    }

    //Main
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("N: ");
        int n = sc.nextInt();
        sc.close();
        int fact = Main.doRecursiveFactorial(n);
        System.out.println("Recursive factorial is: " + fact);
        fact = Main.doSimpleFactorial(n);
        System.out.println("Simple factorial is: " + fact);
    }
}
