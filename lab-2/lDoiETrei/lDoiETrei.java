package g31024.StoianUrzica.TeodorCristian.l2.e3;

import java.util.Scanner;

public class lDoiETrei {
	public static void main(String args[]) {
		int nrPrime = 0;
		Scanner sc = new Scanner(System.in);
		System.out.print("Small number: ");
		int a = sc.nextInt();
		System.out.print("Big number: ");
		int b = sc.nextInt();
		sc.close();
		NaturalNumber nr = new NaturalNumber(a+1);
		while(nr.isLessThan(b)) {
			if (nr.isPrime()) {
				System.out.print(nr.getValue()+", ");
				nrPrime++;
			}
			nr.increment();
		}
		System.out.println();
		System.out.println("There are " + nrPrime + " prime numbers.");
	}
}
