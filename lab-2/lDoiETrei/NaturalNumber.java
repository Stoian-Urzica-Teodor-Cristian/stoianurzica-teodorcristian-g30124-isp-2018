package g31024.StoianUrzica.TeodorCristian.l2.e3;

public class NaturalNumber {
	private int value;
	
	public NaturalNumber(int value) {
		if (value<0) {
			this.value = 0;
		}
		else {
		this.value = value;
		}
	}
	
	public NaturalNumber() {
		this.value = 0;
	}
	
	public void increment(int i) {
		if(value + i >= 0) value+=i;
	}
	
	public void increment() {
		value++;
	}
	
	public int getValue() {
		return value;
	}
	
	public boolean isPrime() {
		if((value == 1) || (value == 0)) return false;
		for(int i = 2; i < value / 2; i++) {
			if (value % i == 0) return false;
		}
		return true;
	}
	
	public boolean isLessThan(int n) {
		if(value < n) return true;
		else return false;
	}
}