package g31023.StoianUrzica.TeodorCristian.l3.e6;

public class TestMyPoint {
    public static void main (String[] args){
        MyPoint a = new MyPoint();
        MyPoint b = new MyPoint(1,2);
        a.setX(2);
        a.setY(2);
        b.setXY(2,1);
        System.out.println("To String de a " + a.toString());
        System.out.println("Distanta de a la b: " + a.distance(b));
        System.out.println("Distanta de a la b (cu coord): " + a.distance(b.getX(),b.getY()));
    }
}
