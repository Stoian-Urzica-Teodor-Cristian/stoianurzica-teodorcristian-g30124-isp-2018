package g31023.StoianUrzica.TeodorCristian.l3.e3;
import becker.robots.*;

public class Exercise3 {
    public static void main(String[] args) {
        City myCity = new City();
        Robot vasilica = new Robot(myCity, 1, 1, Direction.NORTH);
        //miscare spre nord de 5 ori
        for (int step = 0; step < 4; step++)
            vasilica.move();

        //intoarcere vasilica
        vasilica.turnLeft();
        vasilica.turnLeft();

        //miscare robot inapoi
        for (int step = 0; step < 4; step++)
            vasilica.move();
    }
}
