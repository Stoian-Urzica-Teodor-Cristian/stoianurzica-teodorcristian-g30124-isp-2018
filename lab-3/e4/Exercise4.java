package g31023.StoianUrzica.TeodorCristian.l3.e4;

import becker.robots.*;

public class Exercise4 {
    private static void createWalls(City aCity){
        Wall blockAve = new Wall(aCity, 1, 1, Direction.NORTH);
        Wall blockAve2 = new Wall(aCity, 1, 2, Direction.NORTH);
        Wall blockAve3 = new Wall(aCity, 1, 2, Direction.EAST);
        Wall blockAve4 = new Wall(aCity, 2, 2, Direction.EAST);
        Wall blockAve5 = new Wall(aCity, 2, 2, Direction.SOUTH);
        Wall blockAve6 = new Wall(aCity, 1, 1, Direction.WEST);
        Wall blockAve7 = new Wall(aCity, 2, 1, Direction.WEST);
        Wall blockAve8 = new Wall(aCity, 2, 1, Direction.SOUTH);
        //Wall blockAve2 = new Wall(aCity, 1, 2, Direction.EAST);
    }

    public static void main(String[] args){
        City myCity = new City();
        createWalls(myCity);
        Robot vasile = new Robot(myCity, 0, 2, Direction.WEST);

        vasile.move();
        vasile.move();

        vasile.turnLeft();

        vasile.move();
        vasile.move();
        vasile.move();

        vasile.turnLeft();

        vasile.move();
        vasile.move();
        vasile.move();

        vasile.turnLeft();

        vasile.move();
        vasile.move();
        vasile.move();

        vasile.turnLeft();
        vasile.move();

    }
}
