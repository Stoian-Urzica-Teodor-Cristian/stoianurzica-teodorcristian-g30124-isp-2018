
package test;

import java.util.*;

public class Solution {
    public static int getSol(int[] A, int n){
        if (n == 1)
            return A[0];
        int appVec[] = new int[9999999];
        int max = 0;

        for (int i = 0; i<n; i++) {
            appVec[A[i]] = appVec[A[i]] + 1;
            if (max < A[i])
                max = A[i];
        }

        for (int i = 0; i <= max; i++) {
            if (appVec[i] == 1)
                return i;
        }

        return 0;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        System.out.print("Numarul de elemente: ");
        int n = sc.nextInt();

        int vec[] = new int[n];

        for (int i = 0; i<n; i++) {
            System.out.print("Elementul: [" + i + "]: ");
            vec[i] = sc.nextInt();
        }

        int sol = getSol(vec, n);
        System.out.println("Rez: " + sol);
    }
}
