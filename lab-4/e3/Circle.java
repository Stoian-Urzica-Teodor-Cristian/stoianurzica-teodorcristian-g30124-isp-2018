package ex3;

public class Circle {
    private double radius;
    private String color;

    public Circle(){
        this.radius = (double)1;
        this.color = "red";
    }

    public Circle(double radius, String color){
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea(){
        return (3.14 * (this.radius * this.radius));
    }
}
