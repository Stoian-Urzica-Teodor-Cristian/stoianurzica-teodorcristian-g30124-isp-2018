package ex2;

public class MyPoint {
    private int x, y;

    public MyPoint(){
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y){
        this.x = x;
        this.y = y;
    }

    public String toString(){
        return new String("(" + x + "," + y + ')');
    }

    public float distance(int x, int y){
        return ((this.x - x)^2 + (this.y - y)^2) ^(1/2);
    }

    public float distance(MyPoint pct){
        return distance(pct.getX(), pct.getY());
    }
}
