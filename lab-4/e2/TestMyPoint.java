package ex2;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestMyPoint{
    @Test
    public void shouldSetX(){
        MyPoint p = new MyPoint(1,1);
        p.setX(3);
        assertEquals(p.getX(), 3);
    }

    @Test
    public void shouldSetY(){
        MyPoint p = new MyPoint(1,1);
        p.setY(3);
        assertEquals(p.getY(), 3);
    }
}