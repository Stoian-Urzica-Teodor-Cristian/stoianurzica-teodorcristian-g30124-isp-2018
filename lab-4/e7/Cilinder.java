package StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e5;

import StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e1.Circle;

public class Cilinder extends Circle {

    private double heigh;

    public Cilinder(double radius, double heigh, String color){
        super(radius,color);
        this.heigh = heigh;
    }

    @Override
    public double getArea(){
        return this.heigh * 2 * 3.14 * this.getRadius();
    }
}
