package StoianUrzica.TeodorCristian.ISP2018.lab4.e9;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Wall;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import static junit.framework.TestCase.assertEquals;

public class TestDiver {

   private City myCity = new City();
   private Diver jumper = new Diver(this.myCity, 1,3, Direction.NORTH);

    public TestDiver() throws AWTException {
        System.out.println("Bad things happened!");
    }

    @Before
    public void init(){
        Wall block1 = new Wall(myCity, 2, 3, Direction.NORTH);
        Wall block2 = new Wall(myCity, 2, 2, Direction.EAST);
        Wall block3 = new Wall(myCity, 3, 2, Direction.EAST);
        Wall block4 = new Wall(myCity, 4, 2, Direction.EAST);
        Wall block5 = new Wall(myCity, 4, 3, Direction.WEST);
        Wall block6 = new Wall(myCity, 4, 3, Direction.SOUTH);
        Wall block7 = new Wall(myCity, 4, 4, Direction.SOUTH);
        Wall block8 = new Wall(myCity, 4, 4, Direction.EAST);
    }

       @Test
    public void testDive(){
        jumper.dive();
        assertEquals(jumper.getAvenue(), 4);
           assertEquals(jumper.getDirection(), Direction.NORTH);
    }

}
