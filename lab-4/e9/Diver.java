package StoianUrzica.TeodorCristian.ISP2018.lab4.e9;

import java.awt.*;
import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;

public class Diver extends Robot {
    public Diver(City var1, int var2, int var3, Direction var4) throws AWTException {
        super(var1, var2, var3, var4);
    }

    public void dive(){
        super.move();
        super.turnLeft();
        super.turnLeft();
        super.turnLeft();
        super.move();
        super.turnLeft();
        super.turnLeft();
        super.turnLeft();
        super.move();
        super.turnLeft();
        super.turnLeft();
        super.turnLeft();
        super.turnLeft();
        super.move();
        super.move();
        super.move();
        super.turnLeft();
        super.turnLeft();
    }
}
