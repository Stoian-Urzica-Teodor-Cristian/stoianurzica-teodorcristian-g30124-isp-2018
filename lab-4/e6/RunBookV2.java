package StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e4;

import StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e2.Author;
import StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e3.Book;

import java.util.ArrayList;

public class RunBookV2 {
    public static void main(String[] args){
        Author a1 = new Author("Alin", "alin@domain.com", 'm');
        Author a2 = new Author("Magda", "magda@domain.com", 'f');
        ArrayList<Author> authors = new ArrayList<Author>();
        authors.add(a1);
        authors.add(a2);
        BookV2 b1 = new BookV2("MultiBook", authors, 20);
        System.out.println(b1.toString());
    }
}
