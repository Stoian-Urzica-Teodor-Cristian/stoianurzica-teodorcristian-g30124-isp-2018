package StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e4;

import StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e2.Author;

import java.util.ArrayList;

public class BookV2 {
    private String name;
    private ArrayList<Author> authors;
    private double price;
    private int qtyInStock;

    public BookV2(String name, ArrayList<Author> authors, double price){
        this.authors = authors;
        this.price = price;
        this.name = name;
        this.qtyInStock = 0;
    }

    public BookV2(String name, ArrayList<Author> authors, double price, int qtyInStock){
        this.authors = authors;
        this.price = price;
        this.name = name;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Author> getAuthors() {
        return this.authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return this.name + " by " + this.authors.size() + " authors.";
    }
}
