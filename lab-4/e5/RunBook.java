package StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e3;

import StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e2.Author;

public class RunBook {
    public static void main(String[] args){
        Author a1 = new Author("Alin", "alin@domain.com", 'm');
        Book b1 = new Book("AlinsBook", a1, 20);
        System.out.println(b1.toString());
    }
}
