package StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e3;

import StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e2.Author;

public class Book {
    private String name;
    private Author author;
    private double price;
    private int qtyInStock;

    public Book(String name, Author author, double price){
        this.author = author;
        this.price = price;
        this.name = name;
        this.qtyInStock = 0;
    }

    public Book(String name, Author author, double price, int qtyInStock){
        this.author = author;
        this.price = price;
        this.name = name;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return this.name + " by " + this.author.toString();
    }
}
