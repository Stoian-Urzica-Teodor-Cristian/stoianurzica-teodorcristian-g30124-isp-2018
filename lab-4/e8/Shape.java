package StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e6;

public class Shape {
    private String color;
    private boolean filled;

    public Shape(){
        this.color = "green";
        this.filled = true;
    }

    public Shape(String color, boolean filled){
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        if (this.filled == true)
            return "A Shape with color of " + this.color + " and filled.";
        else
            return "A Shape with color of " + this.color + " and NOT filled.";
    }
}
