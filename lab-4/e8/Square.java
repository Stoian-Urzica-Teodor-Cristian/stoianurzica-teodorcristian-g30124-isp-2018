package StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e6;

public class Square extends Rectangle{

    public Square(){
        super();
    }

    public Square(double side){
        super(side, side);
    }

    public Square(String color, boolean filled, double side){
        super(color, filled, side, side);
    }

    @Override
    public void setLength(double length){
        super.setLength(length);
        super.setWidth(length);
    }

    @Override
    public void setWidth(double width){
        super.setLength(width);
        super.setWidth(width);
    }

    @Override
    public String toString(){
        return "A square with side = " + this.getLength() + ", wich is a subclass of " + super.toString();
    }
}
