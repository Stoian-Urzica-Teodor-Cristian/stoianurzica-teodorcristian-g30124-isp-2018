package StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e6;

public class RunShapes {
    public static void main(String[] args){
        CircleV2 c1 = new CircleV2(1);
        System.out.println(c1.toString());
        System.out.println("Circle's area =  " + c1.getArea());

        Rectangle r1 = new Rectangle(2,5);
        System.out.println(r1.toString());
        r1.setWidth(4);
        r1.setFilled(false);
        System.out.println(r1.toString());

        Square sq1 = new Square(2);
        sq1.setLength(20);
        sq1.setColor("Magenta");
        System.out.println(sq1.toString());

        sq1.setWidth(5);
        System.out.println("Square's area = " + sq1.getArea());
        System.out.println("Square's perimeter = " + sq1.getPerimeter());
    }
}
