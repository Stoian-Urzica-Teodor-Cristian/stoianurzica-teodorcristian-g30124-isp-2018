package StoianUrzica.TeodorCristian.ISP2018.lab4Old.e1.e6;

public class CircleV2 extends Shape {
    private double radius;

    public CircleV2(){
        super();
        this.radius = 1.0;
    }

    public CircleV2(double radius){
        super();
        this.radius = radius;
    }

    public CircleV2(double radius, String color, boolean filled){
        super(color,filled);
        this.radius = radius;
    }

    public double getArea(){
        return (3.14 * (this.radius * this.radius));
    }

    public double getPerimeter(){
        return 4 * 3.14 * this.radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString(){
        return "A Circle with radius = " + this.radius + " wich is a subclass of " + super.toString();
    }
}
