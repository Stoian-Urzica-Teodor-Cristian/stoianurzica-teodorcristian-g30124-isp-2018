package StoianUrzica.TeodorCristian.ISP2018.lab8.e2;

import javax.swing.*;

public class Screen extends JFrame {
    private int i = 0;
    protected JButton buton;
    protected JLabel numberPanel;

    public Screen(){
        setTitle("Slow Click Contest");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,300);
        setVisible(true);
    }

    public Integer getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public JLabel getNumberPanel() {
        return numberPanel;
    }

    private void init(){
        this.setLayout(null);
        int width = 150; int height = 20;
        numberPanel = new JLabel();
        numberPanel.setBounds(10,50,width,height);
        numberPanel.setText(Integer.toString(i));
        buton = new JButton("Don't click!");
        buton.setBounds(20,100,width,height);

        buton.addActionListener(new NumberButtonListener(this));
        add(buton);
        add(numberPanel);
    }

    public static void main(String[] args) {
        new Screen();
    }
}
