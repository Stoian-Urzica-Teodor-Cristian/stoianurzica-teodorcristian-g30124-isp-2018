package StoianUrzica.TeodorCristian.ISP2018.lab8.e2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NumberButtonListener implements ActionListener {

    private Screen inFrame;

    public NumberButtonListener(Screen inFrame){
        this.inFrame = inFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        inFrame.setI(inFrame.getI()+1);
        inFrame.getNumberPanel().setText(Integer.toString(inFrame.getI()));
    }
}
