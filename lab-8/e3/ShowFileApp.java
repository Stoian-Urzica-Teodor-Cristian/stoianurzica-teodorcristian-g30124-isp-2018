package StoianUrzica.TeodorCristian.ISP2018.lab8.e3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ShowFileApp extends JFrame {
    private String fileContent;
    protected JTextArea fileTextField;
    protected JButton loadingButton;
    protected JTextField fileNameField;

    private String getStringFromFile(String fileName){
        File file = new File("C:\\Users\\Cristian\\Desktop\\JAVA\\ISP2018\\src\\StoianUrzica\\TeodorCristian\\ISP2018\\lab8\\e3\\" + fileName);
        String a = new String();
        StringBuilder aEnc = new StringBuilder();
        try {
            Scanner sc = new Scanner(file);
            a = sc.next();
            System.out.println("Continutul fisierului: " + a);
            sc.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return a;
    }

    private void init() {
        this.setLayout(null);
        loadingButton = new JButton("Load file");
        fileNameField = new JTextField("Introduce file name");
        fileTextField = new JTextArea();
        loadingButton.setBounds(200, 70, 260, 70);
        fileNameField.setBounds(50,70, 150, 70);
        fileTextField.setBounds(50, 180, 800, 500);
        add(loadingButton);
        loadingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileTextField.setText(getStringFromFile("test.txt"));
            }
        });
        add(fileNameField);
        add(fileTextField);
    }

    public ShowFileApp() {
        setTitle("Loadig file app");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(1280, 720);
        setVisible(true);
    }


    public static void main(String[] args) {
        new ShowFileApp();
    }
}