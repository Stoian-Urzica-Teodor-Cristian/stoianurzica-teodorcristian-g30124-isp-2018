package StoianUrzica.TeodorCristian.ISP2018.lab6.e6;


import java.awt.*;

public class Circle extends Shape {

    private int radius;

    public Circle(String id, Color color, int radius) {
        super(id, color);
        this.radius = radius;
    }

    public Circle(String id, Color color, boolean fill, int x, int y, int radius) {
        super(id, color, fill, x, y);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        if (super.fill)
            g.fillOval(this.x, this.y, radius, radius);
        g.drawOval(this.x , this.y ,radius,radius);
    }
}
