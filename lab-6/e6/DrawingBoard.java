package StoianUrzica.TeodorCristian.ISP2018.lab6.e6;


import StoianUrzica.TeodorCristian.ISP2018.lab6.e6.Shape;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

    //Shape[] shapes = new Shape[10];
    ArrayList<Shape> shapes = new ArrayList<>();

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(1600,900);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Shape s1){
        shapes.add(s1);
        this.repaint();
    }

    public void removeShape(String id){
        for(Shape sh : shapes)
            if(sh.getId().equals(id))
                shapes.remove(sh);
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.size();i++){
            if(shapes.get(i)!=null)
                shapes.get(i).draw(g);
        }
//        for(Shape s:shapes)
//            s.draw(g);
    }
}
