package StoianUrzica.TeodorCristian.ISP2018.lab6.e6;

import java.awt.*;

/**
 * @author mihai.hulea
**/

public class Main {
    public static void drawFractal(DrawingBoard board, int centerX, int centerY, int radius){
        board.addShape(new Circle("1", Color.ORANGE, false, centerX, centerY, radius));
        if (radius>2) {
            drawFractal(board, centerX-radius/2, centerY, radius / 2);
            drawFractal(board, centerX+radius/2, centerY, radius / 2);

        }
    }
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        drawFractal(b1, 900,100,300);
    }
}
