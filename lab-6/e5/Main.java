package StoianUrzica.TeodorCristian.ISP2018.lab6.e5;

import StoianUrzica.TeodorCristian.ISP2018.lab6.e1.DrawingBoard;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int numberOfBricks, brickL, brickW;
        Scanner sc = new Scanner(System.in);
        System.out.print("Insert number of bricks(must be positive integer value): ");
        numberOfBricks = sc.nextInt();
        System.out.print("Insert lenght of a brick (must be positive integer value): ");
        brickL = sc.nextInt();
        System.out.print("Insert width of a brick (must be positive integer value): ");
        brickW = sc.nextInt();
        PyramidConstructor pc = new PyramidConstructor(numberOfBricks, brickL, brickW);
        ArrayList<Shape> bricks = pc.getPyramid();
        DrawingBoard b1 = new DrawingBoard();
        for(Shape sh : bricks)
            b1.addShape(sh);
    }
}
