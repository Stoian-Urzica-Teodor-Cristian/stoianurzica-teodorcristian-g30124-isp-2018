package StoianUrzica.TeodorCristian.ISP2018.lab6.e5;

import java.awt.*;
import java.util.ArrayList;

public class PyramidConstructor {
    private int numberOfBricks;
    private int x = 0, y = 0, rowsNumber = 0, brickL, brickW, dif = 500;
    private ArrayList<Shape> pyramid = new ArrayList<>();

    public int getNumberOfBricks() {
        return numberOfBricks;
    }

    public PyramidConstructor(int numberOfBricks, int brickL, int brickW){
        this.numberOfBricks = numberOfBricks;
        this.brickL = brickL;
        this.brickW = brickW;
    }

    private void validate(int decrementer){
        System.out.println(decrementer);
        if(numberOfBricks - decrementer >= 0){
            numberOfBricks -= decrementer;
            rowsNumber++;
            decrementer++;
            validate(decrementer);
        }
    }

    public ArrayList<Shape> getPyramid() {
        validate(1);
        System.out.println("Levels: " + rowsNumber);
        System.out.println("Briks dimensions: " + brickL+ " x " + brickW);
        for (int i = rowsNumber-1 ; i >= 0; i--) {
            for (int j = 0; j <= i; j++)
                pyramid.add(new Brick("1", Color.RED, false, j*brickL + dif, i*brickW + 100, brickL, brickW));
            dif += brickL/2;
        }
        System.out.println(this.getNumberOfBricks() + " unused bricks!");
        return this.pyramid;
    }
}
