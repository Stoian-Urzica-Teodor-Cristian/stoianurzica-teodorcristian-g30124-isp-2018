package StoianUrzica.TeodorCristian.ISP2018.lab6.e5;


import java.awt.*;

public abstract class Shape {

    protected int x = 0,y = 0;
    protected String id;
    protected boolean fill;

    private Color color;

    public Shape(String id, Color color) {
        this.id = id;
        this.color = color;
    }

    public Shape(String id, Color color, boolean fill, int x, int y){
        this.color = color;
        this.x = x;
        this.y = y;
        this.fill = fill;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);

    public String getId() {
        return id;
    }
}
