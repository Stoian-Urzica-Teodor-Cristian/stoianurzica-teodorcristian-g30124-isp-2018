package StoianUrzica.TeodorCristian.ISP2018.lab6.e5;


import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

    ArrayList<Shape> shapes = new ArrayList<>();

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Shape s1){
        shapes.add(s1);
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.size();i++){
            if(shapes.get(i)!=null)
                shapes.get(i).draw(g);
        }
    }
}
