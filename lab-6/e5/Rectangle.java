package StoianUrzica.TeodorCristian.ISP2018.lab6.e5;


import StoianUrzica.TeodorCristian.ISP2018.lab6.e1.Shape;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;

    public Rectangle(String id, Color color, boolean fill, int length) {
        super(id, color);
        this.length = length;
    }

    public Rectangle(String id, Color color, boolean fill, int x, int y, int length) {
        super(id, color, fill, x, y);
        this.length = length;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        if (super.fill)
            g.fillRect(super.x, super.y, this.length, this.length);
        g.drawRect(super.x, super.y, this.length, this.length);
    }
}
