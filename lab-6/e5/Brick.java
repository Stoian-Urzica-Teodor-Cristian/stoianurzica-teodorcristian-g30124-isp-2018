package StoianUrzica.TeodorCristian.ISP2018.lab6.e5;

import java.awt.*;

public class Brick extends Shape {

        private int length, width;
        private Color col;

        public Brick(String id, Color color, boolean fill, int length, int width) {
            super(id, color);
            this.length = length;
            this.width = width;
            this.col = color;
        }

        public Brick(String id, Color color, boolean fill, int x, int y, int length, int width) {
            super(id, color, fill, x, y);
            this.length = length;
            this.width = width;
        }

        @Override
        public void draw(Graphics g) {
            //System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
            g.setColor(getColor());
            super.setColor(Color.blue);
            if (super.fill)
                g.fillRect(super.x, super.y, this.length, this.width);
            super.setColor(this.col);
            g.drawRect(super.x, super.y, this.length, this.width);
        }

}
