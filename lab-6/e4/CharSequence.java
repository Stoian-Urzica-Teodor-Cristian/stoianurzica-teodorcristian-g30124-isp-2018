package StoianUrzica.TeodorCristian.ISP2018.lab6.e4;

public interface CharSequence {
    public char charAt(int i);
    public int length();
    public CharSequence subSequnce(int start, int end);
    public String toString();
}
