package StoianUrzica.TeodorCristian.ISP2018.lab6.e4;

public class MineCharSequence {
    private String seq;

    public MineCharSequence(String seq){
        this.seq = seq;
    }

    public char charAt(int i){
        return seq.charAt(i);
    }

    public int length(){
        return seq.length();
    }

    public char[] subSequence(int s, int f){
        return seq.substring(s, f).toCharArray();
    }
}
