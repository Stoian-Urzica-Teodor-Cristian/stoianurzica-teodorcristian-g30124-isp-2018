package StoianUrzica.TeodorCristian.ISP2018.lab6.e4.Tests;

import StoianUrzica.TeodorCristian.ISP2018.lab6.e4.MineCharSequence;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestMineCharSequence {
    private MineCharSequence sequence = new MineCharSequence("ABCDEFG");

    @Test
    public void testCharAt(){
        assertEquals('A', sequence.charAt(0));
    }

    @Test
    public void testLength(){
        assertEquals(7, sequence.length());
    }

    @Test
    public void testSubSequence(){
        System.out.println(sequence.subSequence(0,3));
        assertEquals("ABC", new String(sequence.subSequence(0,3)));
    }
}
