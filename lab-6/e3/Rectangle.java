package StoianUrzica.TeodorCristian.ISP2018.lab6.e3;


import java.awt.*;

public class Rectangle implements Shape {

    private int length;
    private Color color;
    private String id;
    private int x, y;
    private boolean fill;

    public Rectangle(String id, Color color, boolean fill, int length) {
        this.id = id;
        this.color = color;
        this.fill = fill;
        this.length = length;
    }

    public Rectangle(String id, Color color, boolean fill, int x, int y, int length) {
        this(id, color, fill, length);
        this.length = length;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        if (fill)
            g.fillRect(x, y, length, length);
        g.drawRect(x, y, length, length);
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }
}
