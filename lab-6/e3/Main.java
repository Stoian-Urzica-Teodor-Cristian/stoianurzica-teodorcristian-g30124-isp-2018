package StoianUrzica.TeodorCristian.ISP2018.lab6.e3;

import java.awt.*;

/**
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle("1", Color.RED, true, 50,50, 90);
        b1.addShape(s1);
        Shape s2 = new Circle("2", Color.GREEN, false, 50,50, 100);
        b1.addShape(s2);
        Shape s3 = new Rectangle("3",Color.ORANGE, false, 50,50,200);
        b1.addShape(s3);

    }
}
