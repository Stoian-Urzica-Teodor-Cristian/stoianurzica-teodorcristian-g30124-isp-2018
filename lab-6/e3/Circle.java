package StoianUrzica.TeodorCristian.ISP2018.lab6.e3;


import java.awt.*;

public class Circle implements Shape {

    private int radius;
    private Color color;
    private String id;
    private int x, y;
    private boolean fill;

    public Circle(String id, Color color, int radius) {
        this.id = id;
        this.color = color;
        this.radius = radius;
    }

    public Circle(String id, Color color, boolean fill, int x, int y, int radius) {
        this(id, color, radius);
        this.x = x;
        this.y = y;
        this.fill = fill;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        if (fill)
            g.fillOval(this.x, this.y, radius, radius);
        g.drawOval(this.x , this.y ,radius,radius);
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String getId() {
        return id;
    }
}
