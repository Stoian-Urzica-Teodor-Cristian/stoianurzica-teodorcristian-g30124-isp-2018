package StoianUrzica.TeodorCristian.ISP2018.lab6.e3;

import java.awt.*;

public interface Shape {

    public Color getColor();

    public void setColor(Color color);

    public abstract void draw(Graphics g);

    public String getId();
}